<?php
/**
 * @file
 *  Implementation of the Google Publisher Tags (ASync)
 */

$plugin = array(
  'title' => t('Google Publisher Tags (ASync)'),
  'defaults' => array(
    'account' => '',
  ),
  'admin form' => 'google_admanager_gpt_async_admin_form',
  'init' => 'google_admanager_gpt_async_init',
  'display ad' => 'google_admanager_gpt_async_display_ad'
);

function google_admanager_gpt_async_admin_form(&$form_state, $conf) {
  $form = array();
  
  $form['account'] = array(
    '#type' => 'textfield',
    '#title' => t('DFP Network Code'),
    '#default_value' => $conf['account'],
    '#size' => 30,
    '#maxlength' => 40,
  );
  
  return $form;
}

function google_admanager_gpt_async_init($conf) {
  $script = <<<EOL
var googletag = googletag || {};
googletag.cmd = googletag.cmd || [];
(function() {
  var gads = document.createElement("script");
  gads.async = true;
  gads.type = "text/javascript";
  var useSSL = "https:" == document.location.protocol;
  gads.src = (useSSL ? "https:" : "http:") + "//www.googletagservices.com/tag/js/gpt.js";
  var node =document.getElementsByTagName("script")[0];
  node.parentNode.insertBefore(gads, node);
 })();
EOL;
  google_admanager_add_js($script, 'init');
  
  $script = "googletag.cmd.push(function() {\n";
  
  // Build variables to add to each zone
  $targetting = array();
  foreach (google_admanager_get_variables() as $key => $value) {
    $targetting[] = "    .setTargeting('{$key}', '{$value}')";
  }
  $targetting = !empty($targetting) ? "\n" . implode("\n", $targetting) : '';
  
  foreach (_google_admanager_get_ad_definitions() as $slot) {
    $dimensions = '[' . $slot['width'] . ', ' . $slot['height'] . ']';
    $script.= <<<EOL

  googletag.defineSlot("/{$conf['account']}/{$slot['name']}", $dimensions, "gam-holder-{$slot['name']}")
    .addService(googletag.pubads()){$targetting};
EOL;
  }
  
  $script.= <<<EOL

  googletag.pubads().collapseEmptyDivs();
  googletag.pubads().enableSingleRequest();
  googletag.enableServices();
});
EOL;
  
  google_admanager_add_js($script, 'slot');
}

function google_admanager_gpt_async_display_ad($ad_slot, $conf) {
  $script = <<<EOL
  googletag.cmd.push(function() {
    googletag.display("gam-holder-{$ad_slot['name']}");
  });
EOL;
  
  $style = 'display: none;';
  if (variable_get('google_admanager_autodetect', FALSE)) {
    if (preg_match('/(\d+)x(\d+)(_.*|)$/', $ad_slot['name'], $match)) {
      $style = 'width:'. $match[1] .'px;height:'. $match[2] .'px;';
    }
  }
  return '<div id="gam-holder-'. $ad_slot['name'] .'" class="gam-holder" style="'. $style .'"><script type="text/javascript">'. $script .'</script></div>';
}