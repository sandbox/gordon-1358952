<?php

/**
 * @file
 */

$plugin = array(
  'title' => t('Google Ad Manager'),
  'defaults' => array(
    'account' => 'ca-pub-',
    'lazy' => FALSE,
  ),
  'admin form' => 'google_admanager_gam_admin_form',
  'admin validate' => 'google_admanager_gam_admin_validate',
  'init' => 'google_admanager_gam_init',
  'display ad' => 'google_admanager_gam_display_ad'
);

function google_admanager_gam_admin_form(&$form_state, $conf) {
  $form = array();
  
  $form['account'] = array(
    '#type' => 'textfield',
    '#title' => t('DFP Property Code'),
    '#default_value' => $conf['account'],
    '#size' => 30,
    '#maxlength' => 40,
    '#description' => t('See detailed instruction in README.txt'),
  );
  
  return $form;
}

function google_admanager_gam_admin_validate(&$form, &$form_state) {
  if (!preg_match('/^ca-pub-\d+$/', $form_state['values']['google_admanager_gam']['account'])) {
    form_set_error('google_admanager_account', t('A valid DFP Property Code is case sensitive and formatted like ca-pub-nnnnnnnnnnnnnnnn.'));
  }
}

function google_admanager_gam_init($conf) {
  //drupal_add_js can't load externaljs in 6, but it will in 7. this is a workaround.
  $external_js = '//partner.googleadservices.com/gampad/google_service.js';
  google_admanager_add_js('document.write(unescape("%3Cscript src=\''. $external_js . '\' type=\'text/javascript\'%3E%3C/script%3E"));', 'init');
  google_admanager_add_js('GS_googleAddAdSenseService("'. $conf['account'] .'");', 'service');
  google_admanager_add_js('GS_googleEnableAllServices();', 'service');

  // Add custom variables
  foreach (google_admanager_get_variables() as $key => $value) {
    google_admanager_add_js('GA_googleAddAttr("'. check_plain($key) .'", "'. check_plain($value) .'");', 'attr');
  }

  // set the close script to fetch the ads.
  google_admanager_add_js('GA_googleFetchAds();', 'close');
}

function google_admanager_gam_display_ad($ad_slot, $conf) {

  $script = '<script type="text/javascript">GA_googleFillSlot("'. $ad_slot['name'] .'");</script>';

  google_admanager_add_js('GA_googleAddSlot("'. $conf['account'] .'", "'. $ad_slot['name'] .'");');

  $style = '';
  // if ad slot name has the format some-name_widthxheight_something,
  // set the div dimension
  if (variable_get('google_admanager_autodetect', FALSE)) {
    if (preg_match('/(\d+)x(\d+)(_.*|)$/', $ad_slot['name'], $match)) {
      $style = ' style="width:'. $match[1] .'px;height:'. $match[2] .'px;"';
    }
  }

  return '<div id="gam-holder-'. $ad_slot .'" class="gam-holder"'. $style .'>'. $script .'</div>';
}