<?php

/** @file
 * Admin functionality
 */

/**
 * Menu callback; Delete a superslot
 *
 */
function google_admanager_admin_superslot_delete($name) {
  $superslots = variable_get('google_admanager_superslots', array());
  if (! isset($superslots[$name])) {
    drupal_set_message(t('Superslot %name not found.', array('%name' => $name)));
    $output = '';
  }
  else {
    $output = drupal_get_form('google_admanager_admin_superslot_confirm_delete', $name);
  }

  return $output;
}

/**
 * Form builder; Builds the confirmation form for deleting a superslot.
 */
function google_admanager_admin_superslot_confirm_delete(&$form_state, $name) {
  $form = array();
  $form['#superslot'] = $name;
  return confirm_form($form, t('Are you sure you want to delete the superslot %name?', array('%name' => $name)), 'admin/settings/google_admanager/superslot');
}

/**
 * Process google_admanager_admin_superslot_confirm_delete form submissions.
 */
function google_admanager_admin_superslot_confirm_delete_submit($form, &$form_state) {
  $superslots = variable_get('google_admanager_superslots', array());
  unset($superslots[$form['#superslot']]);
  variable_set('google_admanager_superslots', $superslots);

  drupal_set_message(t('Superslot %name has been deleted.', array('%name' => $form['#superslot'])));
  $form_state['redirect'] = 'admin/settings/google_admanager/superslot';
}

/**
 * Implementation of hook_admin_settings() for configuring the module.
 */
function google_admanager_admin_settings_form(&$form_state) {
  ctools_include('dependent');
  ctools_include('plugins');
  $form = array();
  
  $plugins = ctools_get_plugins('google_admanager', 'ad_providers');  
  $methods = array_map('_google_admanager_admin_map_methods', $plugins);
  
  $form['google_admanager_delivery_method'] = array(
    '#type' => 'select',
    '#title' => t('Delivery method'),
    '#default_value' => variable_get('google_admanager_delivery_method', 'gam'),
    '#options' => $methods,
    '#description' => t('Determine the method on how the Ads will be delivered to the site.')
  );
  
  foreach ($plugins as $name => $plugin) {
    if (isset($plugin['admin form']) && function_exists($plugin['admin form'])) {
      $conf = variable_get('google_admanager_' . $name, $plugin['defaults']);
      
      $settings = $plugin['admin form']($form_state, $conf);
      
      foreach (element_children($settings) as $key) {
        $settings[$key] = array_merge($settings[$key], array(
          '#process' => array('ctools_dependent_process'),
          '#dependency' => array('edit-google-admanager-delivery-method' => array($name)),
        ));
      }
      $settings['#tree'] = TRUE;
      $form['google_admanager_' . $name] = $settings;
    }
  }
  
  $form_state['storage']['ad_slots'] = isset($form_state['storage']['ad_slots']) ? $form_state['storage']['ad_slots'] : variable_get('google_admanager_ad_slots', array_fill(0, 3, array('name' => '', 'width' => '', 'height' => '')));
  if (empty($form_state['storage']['ad_slots'])) {
    $form_state['storage']['ad_slots'] = array_fill(0, 3, array('name' => '', 'width' => '', 'height' => ''));
  }
  $form['google_admanager_ad_slots'] = array(
    '#type' => 'item',
    '#title' => t('Ad slots'),
    '#tree' => TRUE,
    '#theme' => 'google_admanager_ad_slots',
    '#element_validate' => array('_google_admanager_admin_settings_ad_slots_clean'),
  );
  foreach ($form_state['storage']['ad_slots'] as $id => $ad_slot) {
    $form['google_admanager_ad_slots'][$id]['name'] = array(
      '#type' => 'textfield',
      '#default_value' => !empty($ad_slot['name']) ? $ad_slot['name'] : '',
      '#size' => 60,
    );
    $form['google_admanager_ad_slots'][$id]['width'] = array(
      '#type' => 'textfield',
      '#default_value' => !empty($ad_slot['width']) ? $ad_slot['width'] : '',
      '#size' => 6,
    );
    $form['google_admanager_ad_slots'][$id]['height'] = array(
      '#type' => 'textfield',
      '#default_value' => !empty($ad_slot['height']) ? $ad_slot['height'] : '',
      '#size' => 6,
    );
    $form['google_admanager_ad_slots'][$id]['delete'] = array(
      '#type' => 'submit',
      '#value' => t('delete'),
      '#submit' => array('_google_admanager_admin_settings_del_ad_slot'),
      '#name' => 'google-admanager-ad-slot-delete-' . $id,
      '#key' => $id,
      '#disabled' => count($form_state['storage']['ad_slots']) == 1,
    );
  }
  $form['add_ad_slot'] = array(
    '#type' => 'submit',
    '#value' => t('Add slot'),
    '#submit' => array('_google_admanager_admin_settings_add_ad_slot'),
  );
  
  $form['google_admanager_noblock'] = array(
    '#type' => 'checkbox',
    '#title' => t('Don\'t create blocks'),
    '#default_value' => variable_get('google_admanager_noblock', FALSE),
    '#description' => t('This option allow you to use only superslot. Handful when you have dozens of ad slots or more. <strong>Switch on/off this option may reset blocks positions.</strong>'),
  );
  $form['google_admanager_autodetect'] = array(
    '#type' => 'checkbox',
    '#title' => t('Autodetect ad slot size'),
    '#default_value' => variable_get('google_admanager_autodetect', FALSE),
    '#description' => t('Auto detect ad slot size if name is in format <em>foo_??x??_bar</em> (e.g. <em>homepage_728x90_1</em>). Useful in lazy loading.'),
    '#process' => array('ctools_dependent_process'),
    '#dependency' => array('edit-google-admanager-delivery-method' => array('gam')),
  );
  $form['google_admanager_nodetype_attributes'] = array(
    '#type' => 'checkbox',
    '#title' => t('Expose node type as atttribute'),
    '#default_value' => variable_get('google_admanager_nodetype_attributes', FALSE),
    '#description' => t('This option allows you to target whole content types as an attribute within Google Ad Manager'),
  );
  if (module_exists('taxonomy')) {
    // Get an array of vocabularies
    $vocabs = taxonomy_get_vocabularies();

    // Build the form item defaults
    $vocab_form_item = array(
      '#title' => t('Expose vocabularies as attributes'),
      '#description' => t('Enabling a vocabulary will allow you to target terms within the vocabulary from Google Ad Manager'),
    );

    // If vocabs are empty, insert a prompt form item
    if (empty($vocabs)) {
      $form['google_admanager_vocab_attributes'] = array(
        '#type' => 'item',
        '#value' => '<span class="error">'. t('You must have at least 1 vocabulary for this feature to work.') .'</span>',
      ) + $vocab_form_item;
    }
    else {
      // Build a list of vocabularies as "vid => name" pairs
      $options = array();
      foreach ($vocabs as $v) {
        $options[$v->vid] = $v->name;
      }

      // Create a form item of checkboxes
      $form['google_admanager_vocab_attributes'] = array(
        '#type' => 'checkboxes',
        '#default_value' => variable_get('google_admanager_vocab_attributes', array()),
        '#options' => $options,
      ) + $vocab_form_item;
    }
  }
  
  $form_state['storage']['custom_vars'] = isset($form_state['storage']['custom_vars']) ? $form_state['storage']['custom_vars'] : variable_get('google_admanager_custom_variables', array_fill(0, 3, array('key' => '', 'value' => '')));
  if (empty($form_state['storage']['custom_vars'])) {
    $form_state['storage']['custom_vars'] = array_fill(0, 3, array('key' => '', 'value' => ''));
  }
  $form['custom_variables'] = array(
    '#type' => 'item',
    '#title' => t('Custom Variables'),
  );
  $form['custom_variables']['google_admanager_custom_variables'] = array(
     '#tree' => TRUE,
     '#element_validate' => array('_google_admanager_admin_settings_custom_variables_clean'),
     '#theme' => 'google_admanager_custom_variables',
     '#prefix' => '<div id="google-admanager-admin-settings-form-wrapper">',
     '#suffix' => '</div>',
  );
  
  foreach ($form_state['storage']['custom_vars'] as $key => $vars) {
    $form['custom_variables']['google_admanager_custom_variables'][$key]['key'] = array(
      '#type' => 'textfield',
      '#default_value' => $vars['key'],
      '#size' => 16,
    );
    $form['custom_variables']['google_admanager_custom_variables'][$key]['value'] = array(
      '#type' => 'textfield',
      '#default_value' => $vars['value'],
      '#size' => 64,
    );
    $form['custom_variables']['google_admanager_custom_variables'][$key]['delete'] = array(
      '#type' => 'submit',
      '#value' => t('delete'),
      '#submit' => array('_google_admanager_admin_settings_del_variable'),
      '#name' => 'openx-site-vars-delete-' . $key,
      '#key' => $key,
      '#disabled' => count($form_state['storage']['custom_vars']) == 1,
    );
  }
  
  $form['custom_variables']['add_row'] = array(
    '#type' => 'submit',
    '#value' => t('Add variable'),
    '#submit' => array('_google_admanager_admin_settings_add_variable'),
  );
  $form['custom_variables']['token_vars_tree'] = array(
    '#theme' => 'token_tree',
    '#token_types' => array('node', 'user'),
  );
  
  return system_settings_form($form);
}

function _google_admanager_admin_settings_ad_slots_clean(&$form, &$form_state) {
  $errors = FALSE;

  if (isset($form_state['clicked_button']['#submit'])) {
    return;
  }
  
  $ad_slots = $form_state['values']['google_admanager_ad_slots'];
  foreach ($ad_slots as $key => $var) {
    if (empty($var['name']) && empty($var['width']) && empty($var['height'])) {
      unset($ad_slots[$key]);
    }
    elseif (empty($var['name'])) {
      $errors = TRUE;
      form_error($form[$key]['name'], t('Ad Slot field is required when there is a dimensions are present'));
    }
    else {
      unset($ad_slots[$key]['delete']);
    }
  }
  
  if (!$errors) {
    form_set_value($form, $ad_slots, $form_state);
  }
}

function _google_admanager_admin_settings_del_ad_slot(&$form, &$form_state) {
  $form_state['storage']['ad_slots'] = $form_state['values']['google_admanager_ad_slots'];
  unset($form_state['storage']['ad_slots'][$form_state['clicked_button']['#key']]);
  $form_state['rebuild'] = 1;
}

function _google_admanager_admin_settings_add_ad_slot(&$form, &$form_state) {
  $form_state['storage']['ad_slots'] = $form_state['values']['google_admanager_ad_slots'];
  $form_state['storage']['ad_slots'][] = array('id' => '', 'name' => '');
  $form_state['rebuild'] = 1;
}

function _google_admanager_admin_settings_custom_variables_clean(&$form, &$form_state) {
  $errors = FALSE;

  if (isset($form_state['clicked_button']['#submit'])) {
    return;
  }

  $site_vars = $form_state['values']['google_admanager_custom_variables'];
  foreach ($site_vars as $key => $var) {
    if (empty($var['key']) && empty($var['value'])) {
      unset($site_vars[$key]);
    }
    elseif (empty($var['key']) && !empty($var['value'])) {
      $errors = TRUE;
      form_error($form[$key]['key'], t('Key field is required when there is a value present'));
    }
    else {
      unset($site_vars[$key]['delete']);
    }
  }
  
  if (!$errors) {
    form_set_value($form, $site_vars, $form_state);
  }
}

function _google_admanager_admin_settings_del_variable(&$form, &$form_state) {
  $form_state['storage']['custom_vars'] = $form_state['values']['google_admanager_custom_variables'];
  unset($form_state['storage']['custom_vars'][$form_state['clicked_button']['#key']]);
  $form_state['rebuild'] = 1;
}

function _google_admanager_admin_settings_add_variable(&$form, &$form_state) {
  $form_state['storage']['custom_vars'] = $form_state['values']['google_admanager_custom_variables'];
  $form_state['storage']['custom_vars'][] = array('key' => '', 'value' => '');
  $form_state['rebuild'] = 1;
}


/**
 * Implementation of hook_admin_settings_form_validate().
 */
function google_admanager_admin_settings_form_validate($form, &$form_state) {
  ctools_include('plugins');
  
  $plugin = ctools_get_plugins('google_admanager', 'ad_providers', $form_state['values']['google_admanager_delivery_method']);
  
  if (isset($plugin['admin validate']) && function_exists($plugin['admin validate'])) {
    $plugin['admin validate']($form, $form_state);
  }
}

/**
 * Implementation of hook_admin_settings_form_submit().
 */
function google_admanager_admin_settings_form_submit($form, &$form_state) {
  unset($form_state['storage']);  
  $ad_slots = _google_admanager_get_ad_slots();
  $result = db_query("SELECT bid, delta FROM {blocks} WHERE module = 'google_admanager'");
  while ($block = db_fetch_object($result)) {
    //remove the block when it is not in the list anymore
    if (!isset($ad_slots[$block->delta]) && substr($block->delta, 0, 10) !== 'superslot:') {
      db_query("DELETE FROM {blocks} WHERE bid = %d", $block->bid);
    }
  }

  // Remove orphan ad slots in superslots
  $superslots = variable_get('google_admanager_superslots', array());
  $ad_slot_names = array_values($ad_slots);
  foreach ($superslots as $name => $slots) {
    foreach ($slots as $ad_slot => $php) {
      if (! in_array($ad_slot, $ad_slot_names)) {
        unset($superslots[$name][$ad_slot]);
      }
    }
  }
  variable_set('google_admanager_superslots', $superslots);
}

function theme_google_admanager_ad_slots($form) {
  $head = array(
    t('Name'),
    t('Width'),
    t('Height'),
    '',
  );
  $rows = array();
  
  foreach (element_children($form) as $key) {
    $rows[] = array(
      drupal_render($form[$key]['name']),
      drupal_render($form[$key]['width']),
      drupal_render($form[$key]['height']),
      drupal_render($form[$key]['delete']),
    );
  }
  
  return theme('table', $head, $rows);
}

function theme_google_admanager_custom_variables($form) {
  $head = array(
    t('Variable'),
    t('Value'),
    '',
  );
  $rows = array();
  
  foreach (element_children($form) as $key) {
    $rows[] = array(
      drupal_render($form[$key]['key']),
      drupal_render($form[$key]['value']),
      drupal_render($form[$key]['delete']),
    );
  }
  
  return theme('table', $head, $rows);
}

/**
 * Form to manage (add/remove) superslot
 */
function google_admanager_admin_superslot_form() {
  $form = array();
  $superslots = variable_get('google_admanager_superslots', array());
  $superslot_list = array();

  foreach ($superslots as $name => $slots) {
    $superslot_list[] = '<li>'. l($name, 'admin/build/block/configure/google_admanager/superslot:'. $name) .' ['. l('Delete', 'admin/settings/google_admanager/superslot/delete/'. $name) .']</li>';
  }

  $form['google_admanager_superslot'] = array(
    '#value' => t('A superslot is a block containing many slots, each slot has its own visibility criteria'),
  );

  $form['google_admanager_superslot_current'] = array(
    '#type' => 'fieldset',
    '#title' => t('Current superslot(s)'),
  );
  $form['google_admanager_superslot_current']['list'] = array(
    '#value' => '<ul>'. implode('', $superslot_list) .'</ul>',
  );

  $form['google_admanager_superslot_new'] = array(
    '#type' => 'fieldset',
    '#title' => t('Create new superslot'),
  );
  $form['google_admanager_superslot_new']['google_admanager_superslot_name'] = array(
    '#type' => 'textfield',
    '#title' => t('Superslot name'),
    '#description' => t('Enter a unique name (only alphanumeric and underscore, 1-20 characters)'),
    '#size' => 30,
  );
  $form['google_admanager_superslot_new']['google_admanager_superslot_create'] = array(
    '#type' => 'submit',
    '#value' => t('Create'),
  );

  return $form;
}

/**
 * Validator for google_admanager_admin_superslot_form
 */
function google_admanager_admin_superslot_form_validate($form, &$form_state) {
  $superslots = variable_get('google_admanager_superslots', array());
  if (!preg_match('/^[a-zA-Z0-9_]{1,20}$/', $form_state['values']['google_admanager_superslot_name'])) {
    form_set_error('google_admanager_superslot_name', t('Superslot name can contain only alphanumeric and underscore, 1-20 characters.'));
  }
  if (isset($superslots[$form_state['values']['google_admanager_superslot_name']])) {
    form_set_error('google_admanager_superslot_name', t('Duplicate superslot name.'));
  }
}

/**
 * Submitter for google_admanager_admin_superslot_form
 */
function google_admanager_admin_superslot_form_submit($form, &$form_state) {
  $superslots = variable_get('google_admanager_superslots', array());
  $superslots[$form_state['values']['google_admanager_superslot_name']] = array();
  variable_set('google_admanager_superslots', $superslots);
}

/**
 * Callback for hook_block()
 */
function _google_admanager_block($op = 'list', $delta = 0, $edit = array()) {
  if ($op == 'list') {
    $blocks = array();
    if (! variable_get('google_admanager_noblock', FALSE)) {
      $ad_slots = _google_admanager_get_ad_slots();
      foreach ($ad_slots as $delta => $name) {
        $blocks[$delta] = array(
          'info' => 'GAM Ad slot: '. $name,
          'cache' => BLOCK_NO_CACHE,
        );
      }
    }

    $superslots = variable_get('google_admanager_superslots', array());
    foreach ($superslots as $name => $slots) {
      $blocks['superslot:'. $name] = array(
        'info' => 'GAM Superslot: '. $name,
        'cache' => BLOCK_NO_CACHE,
      );
    }
    return $blocks;
  }
  elseif ($op == 'configure') {
    // Reuse the 'use PHP for block visibility' from block.module
    if (!user_access('use PHP for block visibility') || substr($delta, 0, 10) !== 'superslot:') {
      return;
    }

    $superslots = variable_get('google_admanager_superslots', array());
    $name = substr($delta, 10);
    if (! isset($superslots[$name])) {
      return;
    }

    $form = array();
    $form['visibility'] = array(
      '#type' => 'fieldset',
      '#title' => t('Ad slots visibility'),
      '#description' => t('Use PHP code to define ad slot visibility. For example, to display an ad slot only to anonymous user, use <em>return empty($GLOBALS[\'user\']->uid);</em>. Or, to simple enable an ad slot, use <em>return TRUE;</em>'),
    );

    $ad_slots = array_values(_google_admanager_get_ad_slots());
    $ad_slots = array_combine($ad_slots, $ad_slots);
    $superslot = $superslots[$name];

    // Create 5 empty slots configuration
    // @TODO: It could be better to implement AHAH form
    $superslot += array('fake slot1' => '', 'fake slot2' => '', 'fake slot3' => '');
    $i = 1;
    foreach ($superslot as $ad_slot => $php) {
      $form['visibility']['superslot_'. $i .'_adslot'] = array(
        '#type' => 'select',
        '#title' => t('Ad slot'),
        '#default_value' => $ad_slot,
        '#options' => $ad_slots,
      );
      $form['visibility']['superslot_'. $i++ .'_php'] = array(
        '#type' => 'textfield',
        '#title' => t('PHP code for visibility condition'),
        '#default_value' => $php,
      );
    }

    return $form;
  }
  elseif ($op == 'save') {
    if (!user_access('use PHP for block visibility') || substr($delta, 0, 10) !== 'superslot:') {
      return;
    }

    // When save account settings, delete blocks which belongs to the ad slots that have been removed
    $superslot = array();
    foreach ($edit as $key => $value) {
      if (preg_match('/superslot_(\d+)_adslot/', $key)) {
        $php = $edit[str_replace('adslot', 'php', $key)];
        if (! empty($php)) {
          $superslot[$value] = $php;
        }
      }
    }
    $superslots = variable_get('google_admanager_superslots', array());
    $superslots[substr($delta, 10)] = $superslot;
    variable_set('google_admanager_superslots', $superslots);
  }
}

function _google_admanager_admin_map_methods($a) {
  return $a['title'];
}